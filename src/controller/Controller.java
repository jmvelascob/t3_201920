package controller;

import java.util.Scanner;


import model.data_structures.Node;

import model.logic.MVCModelo;
import model.logic.UBERTrip;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";
		UBERTrip arreglo[]=null;

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				modelo.cargar();
				System.out.println("El total de viajes es de: " + modelo.darTamano() );
				UBERTrip primer = modelo.darlista().darPrimerNodo().darData();
				UBERTrip ultimo = modelo.darlista().darUltimoNodo().darData();
				System.out.println("Primer Viaje->   Zona Origen: " + primer.darSourceid() + "    Zona Destino: " + primer.darDtid() + "    Hora: " + primer.darHod() + " Tiempo Promedio: " + primer.darMean_travel_time());
				System.out.println("Ultimo Viaje->   Zona Origen: " + ultimo.darSourceid() + " Zona Destino: " + ultimo.darDtid() + " Hora: " + ultimo.darHod() + " Tiempo Promedio: " + ultimo.darMean_travel_time());
				System.out.println();
				break;

			case 2:
				System.out.println("Ingrese la hora deseada:");
				dato=lector.next();
				int param= Integer.parseInt(dato);
				arreglo= modelo.consultarViajesHora(param);
				System.out.println("El n�mero de viajes a esta hora es:"+arreglo.length);
				break;
				
			case 3:

				System.out.println("Primeros 10 viajes:");
				long tiempoComienzo=System.currentTimeMillis();
				Comparable[] nuevoarreglo= modelo.shellsort(arreglo);
				long tiempo= System.currentTimeMillis()-tiempoComienzo;
				int cont=0;
				while(cont<10)
				{
					System.out.println(((UBERTrip) nuevoarreglo[cont]).darMean_travel_time());
					cont++;
				}
				System.out.println("Ultimo 10 viajes:");
				int c = 0;
				while(c<10)
				{
					System.out.println(((UBERTrip) nuevoarreglo[nuevoarreglo.length-1-c]).darMean_travel_time());
					c++;
				}
				System.out.println("");
				System.out.println("Se demor�: "+tiempo+" milisegundos en hacer la operaci�n");
				break;
			
			case 4:
				
				System.out.println("Primeros 10 viajes:");
				long tiempoo=System.currentTimeMillis();
				Comparable arreglomerge[]= modelo.mergeSort(arreglo);
				long tiempoo2= System.currentTimeMillis()-tiempoo;
				int contt=0;
				while(contt<10)
				{
					System.out.println(((UBERTrip) arreglomerge[contt]).darMean_travel_time());
					contt++;
				}
				System.out.println("Ultimo 10 viajes:");
				int cc = 0;
				while(cc<10)
				{
					System.out.println(((UBERTrip) arreglomerge[arreglomerge.length-1-cc]).darMean_travel_time());
					cc++;
				}
				System.out.println("");
				System.out.println("Se demor�: "+tiempoo2+" milisegundos en hacer la operaci�n");
				break;
				
			case 5: 
					System.out.println("Primero 10 viajes:");
					long t1=System.currentTimeMillis();
					modelo.quickSort(arreglo);
					long t2= System.currentTimeMillis()-t1;
					int contador=0;
					while(contador<10)
					{
						System.out.println(((UBERTrip) arreglo[contador]).darMean_travel_time());
						contador++;
					}
					System.out.println("Ultimo 10 viajes:");
					int ccc = 0;
					while(ccc<10)
					{
						System.out.println(((UBERTrip) arreglo[arreglo.length-1-ccc]).darMean_travel_time());
						ccc++;
					}
					System.out.println("");
					System.out.println("Se demor�: "+t2+" milisegundos en hacer la operaci�n");
					
					break;	

			default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
