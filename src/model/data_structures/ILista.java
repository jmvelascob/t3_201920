package model.data_structures;

public interface ILista <T extends Comparable<T>> {
	

	/**
//	 * Retornar el numero de elementos presentes en la lista
//	 * @return
//	 */
	int darTamano( );

	void insertar( T dato ); 
	
	void insertaralfinal (T nodo);

	T buscar(T dato);

	Node<T> darUltimoNodo();
	
	boolean esVacio();
	
	Node<T> darPrimerNodo();
	
	
}
