package model.data_structures;



	public class Node<T>
	{
		 T data;
		
		 Node<T> siguiente;
		
		
		public Node(T dato, Node<T> siguientee)
		{
			data= dato;
			siguiente=siguientee;
		}
		
		public Node<T> darSiguiente()
		{
			return siguiente;
		}
		
		public T darData()
		{
			return data;
		}
		
	}	

