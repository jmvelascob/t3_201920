package model.data_structures;

import model.logic.UBERTrip;

public class Lista  <T extends Comparable<T>>
	{
		
		private Node<T> primerNodo;
		
		private int numelementos;
		
		private Node<T> ultimoNodo;

		
		
		public Lista(  )
		{
			primerNodo=null;
			numelementos=0;
			ultimoNodo=null;
		}
		
		public Node<T> darPrimerNodo()
		{
			return primerNodo;
		}
		
		public Node<T> darUltimoNodo()
		{
			return ultimoNodo;
		}
		
		public boolean esVacio()
		{
			return primerNodo==null;
		}
		
		public int darTamano()
		{
			return numelementos;
		}

		public void insertar (T nodo)
		{
			Node<T>nuevo= new Node<T>(nodo, null);
			nuevo.siguiente=primerNodo;
			primerNodo= nuevo;
			this.numelementos++;
		}
		
		public void insertaralfinal (T nodo)
		{
			Node<T>nuevo= new Node<T>(nodo, null);
			Node<T> temporal=primerNodo;
			if(temporal==null)
			{
				primerNodo= nuevo;
				ultimoNodo=nuevo;
				this.numelementos++;
			}
			else
			{
				ultimoNodo.siguiente=nuevo;
				ultimoNodo=nuevo;
				this.numelementos++;
			}
		}
		
		public T buscar(T dato)
		{
			Node<T> devolver= null;
			Node<T> temp= primerNodo;
			
			while(temp!=null && !temp.data.equals(dato))
			{
				temp=temp.siguiente;
			}
			if (temp!=null)
			{
				devolver=temp;
			}
			return devolver.data;

		}
		
		

		}
		

		
	
		
		


