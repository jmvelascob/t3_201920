package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVReader;

import model.data_structures.Lista;
import model.data_structures.Node;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo {
	/**
	 * Atributos del modelo del mundo
	 */
	private Lista<UBERTrip> datos;
	

	public MVCModelo()
	{
		datos = new Lista<UBERTrip>();
	}
	

	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */
	public int darTamano()
	{
		return datos.darTamano();
	}

	/**
	 * Requerimiento de agregar dato
	 * @param dato
	 */
	public void agregar(UBERTrip dato)
	{	
		datos.insertaralfinal(dato);
	}
	
	/**
	 * Requerimiento buscar dato
	 * @param dato Dato a buscar
	 * @return dato encontrado
	 */
	public UBERTrip buscar(UBERTrip dato)
	{
		return datos.buscar(dato);
	}
	


	public void cargar()
		{
			CSVReader reader = null;
			try {
				reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-2-All-HourlyAggregate.csv"));
				try{
				String[] x =reader.readNext();
				}
				catch(Exception e)
				{
					System.out.println("Problemas leyendo la primera linea");
				}
				for(String[] nextLine : reader) 
				{
				    UBERTrip dato = new UBERTrip( Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
				    datos.insertaralfinal(dato);
			    }
			}
			catch (FileNotFoundException e) 
			{
				e.printStackTrace();
			} 
			finally
			{
				if (reader != null) 
				{
					try 
					{
						reader.close();
					} catch (IOException e) 
					{
						e.printStackTrace();
					}
				}

			}
		}
	
	public Lista<UBERTrip> darlista()
	{
		return datos;
	}
	
	public UBERTrip[] consultarViajesHora(int hora)
	{
		Node<UBERTrip> temporal=datos.darPrimerNodo();
		Lista<UBERTrip> arreglo= new Lista<UBERTrip>();
		while(temporal!=null)
		{
			if(temporal.darData().darHod()==hora)
			{
				arreglo.insertaralfinal(temporal.darData());
			}
			temporal=temporal.darSiguiente();
		}
		UBERTrip arr[]= new UBERTrip[arreglo.darTamano()];
		Node<UBERTrip>nod=arreglo.darPrimerNodo();
		int cont=0;
		while(nod!=null)
		{
			arr[cont]=nod.darData();
			cont++;
			nod= nod.darSiguiente();
		}
		return arr;
	}
	

	public Comparable[] shellsort(Comparable arr[])
	{
		int n=arr.length;
		int h=1;
		while(h<n/3)
		{
			h=3*h+1;
		}
		
		while(h>=1)
		{
			for(int i=h; i<n; i++)
			{
				Comparable temp=arr[i];
				
				for(int j=i; j>=h && arr[j].compareTo(arr[j-h])<0; j-=h )
				{
					arr[j]=arr[j-h];
					arr[j-h]=temp;
				}
			}
			h=h/3; 
		}
		return arr;
	}
	
	public Comparable[] mergeSort (Comparable arr[])
	{
		if(arr.length<=1)
		{
			return arr;
		}
		else
		{
			int mitad=arr.length/2;
			Comparable[] arr1= new Comparable[mitad];
			Comparable[] arr2;

			if(arr.length % 2==0)
			{
				arr2=new Comparable[mitad];
			}
			else
			{
				arr2= new Comparable[mitad+1];
			}

			for(int i=0; i<mitad; i++)
			{
				arr1[i]=arr[i];
			}
			for(int i=0; i<arr2.length;i++)
			{
				arr2[i]=arr[mitad+i];
			}
			
			arr1= mergeSort(arr1);
			arr2= mergeSort(arr2);
			Comparable[] dev = merge(arr1, arr2);
			return dev;

		}
	}
			
	

	private Comparable[]  merge(Comparable[] arr1, Comparable[] arr2)
	{
		Comparable[] devolver= new Comparable[arr1.length+arr2.length];
		int punt1=0;
		int punt2=0;
		int finaal=0;
		
		while(punt2<arr2.length||punt1<arr1.length)
		{
			if(punt2<arr2.length && punt1<arr1.length)
			{
				if(arr2[punt2].compareTo(arr1[punt1])<0)
				{
					devolver[finaal]=arr2[punt2];
					finaal++;
					punt2++;
				}
				else{
					devolver[finaal]=arr1[punt1];
					finaal++;
					punt1++;
				}
			}
			else if(punt2<arr2.length)
			{
				devolver[finaal]= arr2[punt2];
				finaal++;
				punt2++;
			}
			else if(punt1<arr1.length)
			{
				devolver[finaal]=arr1[punt1];
				finaal++;
				punt1++;
			}
		}
		return devolver;
	}
	
	//QuickSort
	
	private static void exch(Comparable[] a, int i, int j)
	{
		Comparable swap = a[i];
		a[i] = a[j];
		a[j] = swap;
	}
	
	private static int partition(Comparable[] a, int lo, int hi)
	{
		int i = lo, j = hi+1;
		while (true)
		{
			while (a[++i].compareTo(a[lo])<0)
				if (i == hi) break;
			while (a[lo].compareTo(a[--j])<0)
				if (j == lo) break;

			if (i >= j) break;
			exch(a, i, j);
		}
		exch(a, lo, j);
		return j;
	}
	
	private static void quickSortAlg(Comparable[] a, int lo, int hi)
	 {
		if (hi <= lo) return;
		int j = partition(a, lo, hi);
		quickSortAlg(a, lo, j-1);
		quickSortAlg(a, j+1, hi);
	 }
	
	public void quickSort( Comparable[] a)
	{
		shuffle(a);
		quickSortAlg(a, 0, a.length - 1);
	}
	
	private static void shuffle(Comparable[] a)
	{
		int largo = a.length;
		for(int i=0; i<largo;i++)
		{
			int s = i + (int)(Math.random()* (largo-i));
			Comparable temp = a[s];
			a[s] = a[i];
			a[i] = temp;
		}
	}
	
}
