package model.logic;

public class UBERTrip implements  Comparable<UBERTrip>{
	
	private int sourceid;
	
	private int hod;

	private int dtid;
	
	private double mean_travel_time;
	
	private double standard_deviation_travel_time;
	
	private double geometric_mean_travel_time;
	
	private double geometric_standard_deviation_travel_time;
	
	public UBERTrip(int Psourceid, int Pdtid,int hodd, double Pmean_travel_time, double Pstandard_deviation_travel_time, double Pgeometric_mean_travel_time, double Pgeometric_standard_deviation_travel_time)
	{
		hod= hodd;
		sourceid = Psourceid;
		dtid = Pdtid;
		mean_travel_time = Pmean_travel_time;
		standard_deviation_travel_time = Pstandard_deviation_travel_time;
		geometric_mean_travel_time = Pgeometric_mean_travel_time;
		geometric_standard_deviation_travel_time = Pgeometric_standard_deviation_travel_time;
	}
	
	public int darSourceid()
	{
		return sourceid;
	}
	
	public int darDtid()
	{
		return dtid;
	}
	

	public double darMean_travel_time()
	{
		return mean_travel_time;
	}
	
	public double darStandard_deviation_travel_time()
	{
		return standard_deviation_travel_time;
	}
	
	public double darGeometric_mean_travel_time()
	{
		return geometric_mean_travel_time;
	}
	
	public double darGeometric_standard_deviation_travel_time()
	{
		return geometric_standard_deviation_travel_time;
	}
	public int darHod()
	{
		return hod;
	}


	@Override
	public int compareTo(UBERTrip o) {
		// TODO Auto-generated method stub
		if(mean_travel_time - o.darMean_travel_time()>0)
			return 1;
		else if(mean_travel_time-o.darMean_travel_time()<0)
			return -1;
		else 
		{
			if(standard_deviation_travel_time-o.darStandard_deviation_travel_time()<0)
				return -1;
			else if(standard_deviation_travel_time-o.darStandard_deviation_travel_time()>0)
				return 1;
			else
				return 0;
		}
	}
}
